package org.radic.core;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.EventHandler;

import org.radic.core.commands.CommandHandler;
import org.radic.core.commands.RadicCommand;
import org.radic.core.commands.RadicExtensionsCommand;
import org.radic.core.commands.RadicInfoCommand;

import org.radic.core.utils.Configuration;


public class RadicCore extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {

        Radic.init(((JavaPlugin) this));
        Radic.configuration.load();

        Radic.commands.register("radic", new RadicCommand());
        Radic.commands.register("info", new RadicInfoCommand());
        Radic.commands.register("extensions", new RadicExtensionsCommand());
        Radic.registerCommands();

        getServer().getPluginManager().registerEvents(this, this);
        Radic.console("radic:on-enable", getName());
    }

    @Override
    public void onDisable() {
        Radic.configuration.save();
        Radic.console("radic:on-disable", getName());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Radic.console("radic:on-player-join", e.getPlayer().getName());

    }
}
