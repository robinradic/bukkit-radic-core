package org.radic.core.exceptions;

import java.lang.ArrayIndexOutOfBoundsException;


public class ExtensionNotExistsException extends ArrayIndexOutOfBoundsException {

    public ExtensionNotExistsException() {
        super("Extension could not be found in ExtensionBag.");
    }

    public ExtensionNotExistsException(String name) {
        super("Extension " + name + " could not be found in ExtensionBag");
    }

}
