package org.radic.core.extensions;

/**
 * Created by radic on 7/20/14.
 */
public interface IRadicExtension {
    public String name();
}
