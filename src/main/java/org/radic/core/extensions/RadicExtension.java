package org.radic.core.extensions;

import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.Radic;

import java.io.File;
import java.util.logging.Level;


abstract public class RadicExtension extends JavaPlugin implements IRadicExtension {

    /**
     * Called when this plugin is being enabled
     */
    public void enable() {
    }

    /**
     * Called when this plugin is being disabled
     */
    public void disable() {
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void onEnable() {
        Radic.extensions.add(this);
        this.enable();
        Radic.console("radic:extension:on-enable: " + this.name());
    }

    @Override
    public final void onDisable() {
        Radic.extensions.remove(name());
        this.disable();
        Radic.console("radic:extension:on-disable: " + this.name());
    }

    public final String name(){
        return getName();
    }

    public final String getVersion() {
        return this.getDescription().getVersion();
    }

    public void log(Level level, String message) {
        this.getLogger().log(level, message);
    }

    public void log(String message) {
        this.getLogger().log(Level.INFO, message);
    }


}
