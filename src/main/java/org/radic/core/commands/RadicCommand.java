package org.radic.core.commands;
//Imports for the base command class.

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.Radic;


//This class implements the Command Interface.
public class RadicCommand implements CommandInterface
{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args, JavaPlugin plugin) {
        for (String key : Radic.commands.getCommands().keySet()) {
            Radic.console("radic:command", "radic-command", key);
        }
        return false;
    }

}