package org.radic.core.commands;
//Imports we will need.

import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;


import java.util.HashMap;

//The class will implement CommandExecutor.
public class CommandHandler implements CommandExecutor
{
    private JavaPlugin plugin;
    public CommandHandler(JavaPlugin plugin){
        this.plugin = plugin;
    }
    private static HashMap<String, CommandInterface> commands = new HashMap<String, CommandInterface>();


    public void register(String name, CommandInterface cmd) {
        commands.put(name, cmd);
    }

    public HashMap<String, CommandInterface> getCommands(){
        return commands;
    }

    public boolean exists(String name) {
        return commands.containsKey(name);
    }

    public CommandInterface getExecutor(String name) {
        return commands.get(name);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(args.length == 0) {
            getExecutor("radic").onCommand(sender, cmd, commandLabel, args, this.plugin);
            return true;
        }

       // if(sender instanceof Player) {
            if(args.length > 0) {
                if(exists(args[0])){
                    getExecutor(args[0]).onCommand(sender, cmd, commandLabel, args, this.plugin);
                    return true;
                } else {
                    sender.sendMessage("This command doesn't exist!");
                    return true;
                }
            }
        //} else {
          //  sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
         //   return true;
     //   }
        return false;
    }

}