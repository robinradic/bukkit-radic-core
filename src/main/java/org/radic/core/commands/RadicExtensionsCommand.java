package org.radic.core.commands;
//Imports for the base command class.

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.Radic;


//This class implements the Command Interface.
public class RadicExtensionsCommand implements CommandInterface
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args, JavaPlugin plugin) {

        Radic.console("radic:extensions", Radic.extensions.toString());

        return false;
    }
}