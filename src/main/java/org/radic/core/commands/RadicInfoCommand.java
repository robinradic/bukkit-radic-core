package org.radic.core.commands;
//Imports for the base command class.

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.Radic;


//This class implements the Command Interface.
public class RadicInfoCommand implements CommandInterface
{
    CommandSender sender;
    private void message(String name, String value)
    {
        Radic.message(name, value, sender);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args, JavaPlugin plugin) {
        this.sender = sender;

        sender.sendMessage("onCommand RadicInfoCommand!");
        return false;
    }
}