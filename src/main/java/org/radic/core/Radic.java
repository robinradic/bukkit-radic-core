package org.radic.core;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.radic.core.commands.CommandHandler;

import org.radic.core.commands.RadicCommand;
import org.radic.core.commands.RadicInfoCommand;
import org.radic.core.extensions.ExtensionBag;
import org.radic.core.utils.Configuration;

import java.io.File;

/**
 * Created by radic on 7/20/14.
 */
public class Radic {

    public static JavaPlugin plugin;
    public static Server server;
    public static Configuration configuration;
    public static FileConfiguration config;
    public static File dataFolder;
    public static CommandHandler commands;
    public static ExtensionBag extensions;



    public static void registerCommands() {
        plugin.getCommand("radic").setExecutor(commands);
    }

    public static void init(JavaPlugin plugin)
    {
        Radic.plugin = plugin;
        Radic.server = plugin.getServer();
        Radic.configuration = new Configuration(plugin);
        Radic.config = configuration.getConfig();
        Radic.dataFolder = Radic.plugin.getDataFolder();
        Radic.commands = new CommandHandler(plugin);
        Radic.extensions = ExtensionBag.getInstance();

        console("radic:init", "extensions:count", Radic.extensions.count() + "");

    }

    public static void console(String name, String value)
    {
        server.getConsoleSender().sendMessage(ChatColor.GOLD + "[" + ChatColor.GREEN + name + ChatColor.GOLD + "] " + ChatColor.WHITE + value);
    }

    public static void console(String value)
    {
        server.getConsoleSender().sendMessage(ChatColor.WHITE + value);
    }

    public static void console(String name, String childName, String value)
    {
        server.getConsoleSender().sendMessage(
                ChatColor.GOLD + "[" + ChatColor.GREEN + name + ChatColor.GOLD + "]" +
                ChatColor.WHITE + "->" + ChatColor.AQUA + "(" + ChatColor.YELLOW + childName + ChatColor.AQUA + ") " +
                ChatColor.WHITE + value);
    }

    public static void message(String name, String value, CommandSender sender)
    {

        sender.sendMessage(ChatColor.GOLD + "[ServerInfo " + ChatColor.GREEN + name + ChatColor.GOLD + "] " + ChatColor.WHITE + value);
    }

}
